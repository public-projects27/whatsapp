import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/pages/about.dart';
import 'package:whatsapp/pages/camera_screen.dart';
import 'package:whatsapp/pages/contact_us.dart';
import 'package:whatsapp/pages/image_picker_screen.dart';
import 'package:whatsapp/pages/map_screen.dart';
import 'package:whatsapp/pages/new_group.dart';
import 'package:whatsapp/pages/secend_map_screen.dart';
import 'package:whatsapp/pages/select_language_screen.dart';
import 'package:whatsapp/pages/socket_io_screen.dart';
import 'package:whatsapp/pages/third_map_screen.dart';
import 'package:whatsapp/pages/settings_screen.dart';
import 'package:whatsapp/pages/login_screen.dart';
import 'package:whatsapp/pages/splash.dart';
import 'package:whatsapp/whatsapp_home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await allTranslations.init('fa');
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MyAppState();


}
class MyAppState extends State<MyApp> {

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: ( Map<String, dynamic> message ) {
        print("onMessage : $message");
      },
      onLaunch: ( Map<String, dynamic> message ) {
        print("onLaunch : $message");
      },
      onResume: ( Map<String, dynamic> message ) {
        print("onResume : $message");
      },
    );

    _firebaseMessaging.getToken().then((String token) {
      print("token : $token");
    });
  }


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "What'sApp",
      theme: ThemeData(
        fontFamily: 'Vazir',
        primaryColor: new Color(0xff075e54),
        accentColor: new Color(0xff25d366),
      ),
      localizationsDelegates: [
//        TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: allTranslations.supportedLocales(),
//      supportedLocales: [
//        const Locale('en', ''),
//        const Locale('fa', '')
//      ],
      routes: {
        '/': (context) => prepareScreen(new SplashScreen(), allTranslations.currentLanguage),
        '/image_picker': (context) => prepareScreen(ImagePickerScreen(), allTranslations.currentLanguage),
        '/camera': (context) => prepareScreen(CameraScreen(), allTranslations.currentLanguage),
        '/map1': (context) => prepareScreen(MapScreen(), allTranslations.currentLanguage),
        '/map2': (context) => prepareScreen(MapLocationScreen(), allTranslations.currentLanguage),
        '/map3': (context) => prepareScreen(ThirdMapScreen(), allTranslations.currentLanguage),
        '/home': (context) => prepareScreen(WhatsAppHome(), allTranslations.currentLanguage),
        '/login': (context) => prepareScreen(LoginScreen(), allTranslations.currentLanguage),
        '/settings': (context) => prepareScreen(SettingsScreen(), allTranslations.currentLanguage),
        '/settings/select_language': (context) => prepareScreen(SelectLanguageScreen(), allTranslations.currentLanguage),
//        '/new_chat' : (context) => prepareScreen(CreateChatScreen(), allTranslations.currentLanguage),
        '/new_chat': (context) => prepareScreen(SocketIoScreen(), allTranslations.currentLanguage),
        '/new_group': (context) => prepareScreen(NewGroup(), allTranslations.currentLanguage),
        '/about': (context) => prepareScreen(About(), allTranslations.currentLanguage),
        '/contact_us': (context) => prepareScreen(ContactUs(), allTranslations.currentLanguage),
      }, // for pushes without translate data between layout
      debugShowCheckedModeBanner: false,
    );
  }

  prepareScreen(Widget screen, String currentLanguage) {
    TextDirection textDirection =
    currentLanguage == 'fa' ? TextDirection.rtl : TextDirection.ltr;
    return new Directionality(textDirection: textDirection, child: screen);
  }

}
