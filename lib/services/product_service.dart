import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:whatsapp/Helper.dart';
import 'package:whatsapp/models/Product.dart';
import 'package:whatsapp/sqliteProvider/ProductProvider.dart';


class ProductService {

  static Future<Map> getProducts(int page) async {
    if (await checkConnectionInternet())
      return await _getAllProductsFromNetwork(page);
    else
      return await _getAllProductsFromSqlite(page);
  }




  static Future<Map> _getAllProductsFromNetwork(int page) async {
    final response = await http.get(
//        'http://10.0.2.2/api/products?page=${page}',
      'http://10.1.2.94:8000/api/products?page=${page}',
    );
    if (response.statusCode == 200) {
      var responseBody = json.decode(response.body);

      List<Product> products = [];
      responseBody['data'].forEach((item) {
        products.add(Product.fromJson(item));
      });
      await _saveAllProductsIntoSqlite(products);



      return {
      "currentPage": responseBody['current_page'],
      "products": products
      };
    } else {
      return await _getAllProductsFromSqlite(page);
    }
  }

  static Future<Map> _getAllProductsFromSqlite(int page) async {
    var db = new ProductProvider();
    await db.open();
    List<Product> products = await db.paginate(page);
    await db.close();
    return {
      "currentPage": page,
      "products": products
    };
  }

  static Future<bool> _saveAllProductsIntoSqlite(
      List<Product> products) async {
    var db = new ProductProvider();
    await db.open();
    await db.insertAll(products);
    await db.close();
  }


}