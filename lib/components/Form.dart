import 'package:validators/validators.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/components/InputFields.dart';


class FormContainer extends StatelessWidget {
  final formKey;
  final emailOnSaved;
  final passwordOnSaved;

  FormContainer({@required this.formKey, this.emailOnSaved, this.passwordOnSaved});

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      padding: const EdgeInsets.symmetric(horizontal: 2),
      child: new Column(
        children: <Widget>[
          new Form(
              key: formKey,
              child: new Column(
                children: <Widget>[
                  new InputFieldsArea(
                    defText: 'mshamsi502@gmail.com',
                    hint: allTranslations.text('email'),
                    obscure: false,
                    icon: Icons.person_outline,
                    validator: (String value) {
                      if(! isEmail(value)) {
                        return 'ایمیل معتبر نیس';
                      }
                    },
                    onSaved : emailOnSaved,
                  ),
                  new InputFieldsArea(
                    defText: '123456789',
                    hint:  allTranslations.text('password'),
                    obscure: true,
                    icon: Icons.lock_outline,
                    validator: (String value) {
                      if(value.length < 6)
                        return 'حداقل 6 کاراکتر وارد کن';
                    },
                    onSaved: passwordOnSaved,
                  ),
                ],
              )
          )
        ],
      ),
    );
  }

}