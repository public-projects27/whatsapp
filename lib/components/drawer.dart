

import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/pages/contact_us.dart';
import 'package:whatsapp/pages/map_screen.dart';
import 'package:whatsapp/pages/secend_map_screen.dart';
import 'package:whatsapp/pages/third_map_screen.dart';

Drawer buildDrawerLayout(BuildContext context) {
  return new Drawer(
    child: new ListView(
      children: <Widget>[
        new DrawerHeader(
          padding: EdgeInsets.zero,
            child: new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: <Color>[
                      const Color(0xff075e54),
                      const Color(0xf0128c7e),
                    ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter
                )
              ),
              child: new Stack(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.bottomRight,
                    child: new Padding(
                        padding: const EdgeInsets.only(right: 16, bottom: 16),
                      child: new Text(
                          allTranslations.text("title"),
                          style: TextStyle(
                              fontSize: 22,
                              color: Colors.black,
                              fontWeight: FontWeight.w600
                          ),
                    )
                    ),
                  ),
                  new Align(
                    alignment: Alignment.topRight,
                    child: ListTile(
                      leading: new CircleAvatar(
                        backgroundColor: Colors.grey,
                      ),
                      title: new Text(
                        allTranslations.text("profile"),
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.w400
                        ),
                    ),
                  ),
                  )
                ],
              ),
            ),
        ),
        new ListTile(
          trailing: new Text(allTranslations.text("number_market_basket"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w500,
                fontSize: 15,
              color: Colors.red
            ),
          ),
          leading: new Icon(Icons.shopping_basket),
          title: new Text(allTranslations.text("market_basket"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){},
        ),
        new ListTile(
          trailing: new Text(allTranslations.text("number_products"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.red
            ),
          ),
          leading: new Icon(Icons.shopping_cart),
          title: new Text(allTranslations.text("products"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){},
        ),
        new ListTile(
          leading: new Icon(Icons.location_searching),
          title: new Text(allTranslations.text("map1"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {
            _pushMApScreen(context, 1);
          },
        ),
        new ListTile(
          leading: new Icon(Icons.location_searching),
          title: new Text(allTranslations.text("map2"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {
            _pushMApScreen(context, 2);
          },
        ),
        new ListTile(
          leading: new Icon(Icons.location_searching),
          title: new Text(allTranslations.text("map3"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {
            _pushMApScreen(context, 3);
          },
        ),
        new ListTile(
          leading: new Icon(Icons.call),
          title: new Text(allTranslations.text("contact"),
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){
            Navigator.push(context, new MaterialPageRoute(builder: (context) => ContactUs()));
          },
        ),

      ],
    ),
  );
}

_pushMApScreen(BuildContext context, int mapNum) async {
  if (mapNum == 1)
    await Navigator.push(context,
        new MaterialPageRoute(
            builder: (context) => new MapScreen()
        )
    );
  else if (mapNum == 2)
    await Navigator.push(context,
        new MaterialPageRoute(
            builder: (context) => new MapLocationScreen()
        )
    );
  else if (mapNum == 3)
    await Navigator.push(context,
        new MaterialPageRoute(
            builder: (context) => new ThirdMapScreen()
        )
    );
}