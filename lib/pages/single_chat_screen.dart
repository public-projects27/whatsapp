import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/models/chat_model.dart';
import 'package:whatsapp/pages/camera_screen.dart';

class SingleChatScreen extends StatelessWidget {
  final ChatModel data;
  SingleChatScreen({ @required this.data });

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: new Scaffold(
          appBar: new AppBar(
            automaticallyImplyLeading: false,
            title: new Row(
              children: <Widget>[
                new GestureDetector(
                  child: new Icon(Icons.arrow_back),
                  onTap: () => Navigator.pop(context),
                ),
                new SizedBox(
                  width: 10,
                ),
                new CircleAvatar(
                  backgroundImage: new NetworkImage(data.avatarUrl),
                  backgroundColor: Colors.grey,
                ),
                new SizedBox(
                  width: 10,
                ),
                new Text(this.data.name, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
              ],
            ),
          ),
          body: new Center(
              child: new Padding(
                  padding: const EdgeInsets.only(right: 15, left: 15),
                  child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text('صفحه چت خصوصی با ${this.data.name}', style: TextStyle(fontSize: 18)),
                  new Divider(
                    height: 10,
                  ),
                  new SizedBox(
                    height: deviceSize.height * 0.28,
                  ),
                  new Text( this.data.name + ' : ' , style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  new SizedBox(
                    height: 10,
                  ),
                  new Text( this.data.message , style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300)),
                  new SizedBox(
                    height: deviceSize.height * 0.28,
                  ),
                  new Divider(
                    height: 10,
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new RaisedButton(
                        onPressed: () {
                          Navigator.push(context, new MaterialPageRoute(builder: (context) => CameraScreen() ));
                        },
                        child: new Text('صفحه بعدی', style: TextStyle(fontSize: 16),),
                      ),
                      new RaisedButton(
                        onPressed: () {
                          Navigator.pop(context,' مث اینکه دوس نداری با ${this.data.name} چت کنی :(');
                        },
                        child: new Text(allTranslations.text("back"), style: TextStyle(fontSize: 16),),
                      ),
                    ],
                  ),
                ],
              )
              ),
          ),
        )
    );
  }


}