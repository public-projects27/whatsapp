
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:whatsapp/all_translations.dart';
import 'package:whatsapp/components/widgets/circle_button_widget.dart';

class MapScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MapScreenState();
}

class MapScreenState extends State<MapScreen> {

  GoogleMapController _mapController;
  static const LatLng _center = const LatLng(45.521563, -122.677433);
  MapType _currentMapType = MapType.normal;
  final Set<Marker> _markers = {};
  LatLng _lastMapPosition = _center;
  String locat = 'موقعیت: ';
  Widget centerLocationBar = new Text('موقعیت: ',
                               style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,)
  );

  static final CameraPosition _iranPosition = CameraPosition(
    target: LatLng(36.314, 52.093),
    zoom: 5,
  );
  static final CameraPosition _ITPosition = CameraPosition(
      target: LatLng(35.755189, 51.333883),
      zoom: 19.151926040649414);
  static final CameraPosition _currentLocation = CameraPosition(
      target: LatLng(35.755189, 51.333883),
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {

    void _onCameraMove(CameraPosition position) {
      setState(() {
        _lastMapPosition = position.target;
        locat =  'موقعیت: ${_lastMapPosition.latitude.toStringAsFixed(6)} , ${_lastMapPosition.longitude.toStringAsFixed(6)}';
        centerLocationBar = new Text(
          locat,
          style: TextStyle(
            color: Color(0xff075e54),
            fontWeight: FontWeight.w500,
            fontSize: 11
          ),
        );
      });


    }
    void _onMapCreated(GoogleMapController controller) {
        _mapController = controller;
    }
    void _onMapTypeButtonPressed() {
      setState(() {
        _currentMapType =
        _currentMapType == MapType.normal
            ? MapType.satellite
            : MapType.normal;
      });
    }
    void _onAddMarkerButtonPressed() {
      setState(() {
        _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          position: _lastMapPosition,
          draggable: true,
          infoWindow: InfoWindow(
            title: 'Really cool place',
            snippet: '5 Star Rating',
          ),
          icon: BitmapDescriptor.defaultMarker,
        ));
      });
    }

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(allTranslations.text('map1'),
            style: TextStyle(
            fontFamily: 'Nazanin',
            fontWeight: FontWeight.w500,
            fontSize: 22
        ),
      ),
      ),
      body: new Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          GoogleMap(
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              mapType: _currentMapType,
              initialCameraPosition: _iranPosition,
              markers: _markers,
              onCameraMove: _onCameraMove,
              onMapCreated: _onMapCreated
            ),
          new Row(
            children: <Widget>[
              new Expanded(
                child: new Container(
                  height: 17,
                  decoration:  new BoxDecoration(
                    color: Colors.white54,
                  ),
                  child: new Container(
                    alignment: Alignment.bottomCenter,
                    child: centerLocationBar,
                  ),
                ),
              ),
            ],
          ),
          new Padding(
              padding: const EdgeInsets.only(
                  bottom: 20,
                  left: 20
              ),
              child: new Align(
                alignment: Alignment.bottomLeft,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new CircleButtonWidget(
                      icon: Icons.add_location,
                      color: Colors.black,
                      backgroundColor: Color(0x6607ce55),
                      onTap: _onAddMarkerButtonPressed,
                    ) ,
                    new SizedBox(width: 10,),
                    CircleButtonWidget(
                      icon: Icons.map,
                      color: Colors.black,
                      backgroundColor: Color(0x6607ce55),
                      onTap: _onMapTypeButtonPressed,
                    ) ,
                    new SizedBox(width: 10,),
                    new RaisedButton(
                      color: Colors.white70,
                      onPressed: () {
                        _mapController.animateCamera(
                            CameraUpdate.newCameraPosition(_ITPosition)
                        );
                      },
                      child: new Row(
                        textDirection: TextDirection.rtl,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text('IT AVAKATAN', style: TextStyle(fontSize: 11)),
                          new SizedBox(width: 5),
                          new Icon(Icons.place, color: Color(0xff07ce55),),
                        ],
                      ),
                    ),
                    new SizedBox(width: 10,),
                    CircleButtonWidget(
                      icon: Icons.my_location,
                      color: Colors.black,
                      backgroundColor: Color(0x6607ce55),
                      onTap: (){
                        _mapController.animateCamera(
                            CameraUpdate.newCameraPosition(_currentLocation)
                        );
                      },
                    ) ,
                  ],
                ),
              ),
          ),
        ],
      ),
    );
  }



}



