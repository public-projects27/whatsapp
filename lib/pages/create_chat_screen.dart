import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/pages/camera_screen.dart';

class CreateChatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: new Scaffold(
          appBar: new AppBar(
             title: new Text('ایجاد چت'),
          ),
          body: new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('ایجاد چت جدید', style: TextStyle(fontSize: 20)),
                new Divider(
                  height: 10,
                ),
                new SizedBox(
                  height: deviceSize.height * 0.32,
                ),
                new SizedBox(
                  height: deviceSize.height * 0.32,
                ),
                new Divider(
                  height: 10,
                ),

                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new RaisedButton(
                      onPressed: () {
                        Navigator.push(context, new MaterialPageRoute(builder: (context) => CameraScreen() ));
                      },
                      child: new Text('صفحه بعدی', style: TextStyle(fontSize: 20),),
                    ),
                    new RaisedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('برگشت', style: TextStyle(fontSize: 20),),
                    ),
                  ],
                ),

              ],
            )
//          ),
        )
        )
    );
  }


}