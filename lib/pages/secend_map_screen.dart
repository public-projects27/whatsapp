import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:whatsapp/all_translations.dart';


class MapLocationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MapLocationScreenState();
}

class MapLocationScreenState extends State<MapLocationScreen> {

  LocationData _startLocation;
  LocationData _currentLocation;

  GoogleMapController _mapController;
  CameraPosition _position;

  Location _location = new Location();
  PermissionStatus  _permission;
  LocationData _locationData;
  String error;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initPlatformState();
    _location.onLocationChanged.listen((LocationData currentLocation) {
      setState(() {
        _currentLocation = currentLocation;
      });
      if(_mapController != null) {
        _mapController.animateCamera(CameraUpdate.newCameraPosition(
          new CameraPosition(
              target: LatLng(
                  currentLocation.latitude,
                  currentLocation.longitude
              ),
            zoom: 18
          )
        ));
      }
    });
  }

  void _onCameraMove(CameraPosition position) {
    setState(() {
    });
  }
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _mapController = controller;
    });
  }
  void initPlatformState() async {
    //Map<String, double> location;
    try {
      _permission = await _location.hasPermission();
      _locationData = await _location.getLocation();
      error = null;
    } on PlatformException catch (e) {
      if(e.code == 'PERMISSION_DENIED') {
        error = 'Permission Denied';
      } else if(e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'Permission Denied - Please Ask The User To Enable It From The App Setting';
      }
      _locationData = null;
    }
    setState(() {
      _startLocation = _locationData;
      _position = new CameraPosition(
          target: LatLng(
              _locationData.latitude,
              _locationData.longitude
          ),
        zoom: 18
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    List<Widget> widgets = new List();
    widgets.add(
      new SizedBox(
          width: _screenSize.width,
          height: 300,
          child: _position != null
            ? new GoogleMap(
                onMapCreated: _onMapCreated,
                initialCameraPosition: _position,
            myLocationEnabled: true,
            onCameraMove: _onCameraMove,

                )
            : new SizedBox()
      ),
    );
    widgets.add(
     new Center(
       child: new Text(
           _startLocation != null
               ? 'Start Location:\n ${_startLocation.latitude} ,  ${_startLocation.longitude}'
               : 'Error: \n $error \n',
         textDirection: TextDirection.ltr,
       ),
     )
    );
    widgets.add(
        new Center(
          child: new Text(
              _currentLocation != null
                  ? 'Continuous Location:\n ${_currentLocation.latitude} ,  ${_currentLocation.longitude}'
                  : 'Error: \n $error \n',
            textDirection: TextDirection.ltr,
          ),
        )
    );
    widgets.add(
        new Center(
          child: new Text(
              _permission != null
                  ? 'Has Permission : Yes'
                  : 'Has Permission : Yes'
          ),
        )
    );
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title:new Text(allTranslations.text("map2"),
          style: TextStyle(
              fontFamily: 'Nazanin',
              fontWeight: FontWeight.w500,
              fontSize: 22
          ),
        ),
      ),
      body: new Column(
        children: widgets
      ),
    );
  }







}

