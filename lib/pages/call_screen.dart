import 'package:flutter/cupertino.dart';
import 'package:whatsapp/all_translations.dart';

class CallScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text(allTranslations.text("calls"), style: TextStyle(fontSize: 20),),
    );
  }
}