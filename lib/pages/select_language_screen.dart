import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/all_translations.dart';

class SelectLanguageScreen extends StatelessWidget {
  String currentLanguage;
  SelectLanguageScreen();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('انتخاب زبان'),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
            title: new Text('فارسی'),
            onTap: () {
              _changeLanguage(context, 'fa');
            },
            trailing: checkLanguage('fa')
          ),
          new ListTile(
            title: new Text('English'),
            onTap: () {
              _changeLanguage(context ,'en');
            },
              trailing: checkLanguage('en')
          ),
        ],
      )
    );
  }

  void _changeLanguage(BuildContext context, String lang) async {
    await allTranslations.setNewLanguage(lang);
      Navigator.pushReplacementNamed(context, '/');


  }

  checkLanguage(String lang) {
    return allTranslations.currentLanguage == lang
        ? new Icon(Icons.check)
        : new SizedBox()
    ;
  }


}