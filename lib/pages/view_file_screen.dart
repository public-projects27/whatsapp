
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:video_player/video_player.dart';

class ViewFileScreen extends StatefulWidget {
  final Map file;


  ViewFileScreen({ @required this.file});

  @override
  State<StatefulWidget> createState() => new ViewFileScreenState();
}

class ViewFileScreenState extends State<ViewFileScreen>{

  VideoPlayerController _videoPlayerController;
  bool _isPlaying;
  bool _fullScreen = true;

  @override
  void initState() {
    super.initState();
    if(widget.file['type'] == 'video'){
      _videoPlayerController = VideoPlayerController.file(File(widget.file['path']));
      _videoPlayerController.addListener(() async {

        if(_videoPlayerController.value.position
            >= _videoPlayerController.value.duration) {
          await _videoPlayerController.seekTo(new Duration(milliseconds: 0));
          await _videoPlayerController.pause();
        }

        final bool isPlaying = _videoPlayerController.value.isPlaying;
        if(_isPlaying != isPlaying) {
          setState(() {
            _isPlaying = isPlaying;
          });
        }

        if(mounted) setState(() {});
      });
      _videoPlayerController.initialize().then((_) {
        setState(() {

        });
      });

    }

  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    String _fileName = (widget.file.toString().substring( (widget.file.toString().length)-18, (widget.file.toString().length )-1 ) );
    return new SafeArea(child: new Directionality(
      textDirection: TextDirection.rtl,
      child: new Scaffold(
        backgroundColor: Colors.black87,
//        appBar: ,
//        bottomNavigationBar: ,
        body: new Stack(
          children: [
            new GestureDetector(
              onTap: () {
                setState(() {
                  _fullScreen = ! _fullScreen;
                });
              },
              child: new Center(
                child: widget.file['type'] == 'image'
                    ? _showImage()
                    : widget.file['type'] == 'video'
                    ? _showVideo(context)
                    : null
                ,
              ),
            ),

            new Positioned(
                child: _fullScreen
                ? new Container(width: 0.0, height: 0.0)
                : new Column(
                  children: [
                    new SizedBox(
                      height:  _screenSize.height * 0.1,
                      width: _screenSize.width * 0.95,
                      child: AppBar(
                        primary: false,
                        title: new Directionality(
                            textDirection: TextDirection.ltr,
                            child: new Text(
                                '${_fileName}',
                                style: TextStyle(fontFamily: 'Vazir',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500
                                )
                            )
                        ),
                        backgroundColor: Colors.black12,
                        elevation: 20,
                        actions: <Widget>[
                          new IconButton(
                            icon: Icon(Icons.scatter_plot, color: Colors.purpleAccent,),
                            onPressed: () {
                              print( widget.file.toString().substring( (widget.file.toString().length)-18, (widget.file.toString().length )-1 ));
                            },
                            tooltip: 'More',
                          ),
                        ],
                      ),
                    ),
                    new SizedBox(
                      height: _screenSize.height * 0.75,
                    ),
                    new SizedBox(
                      height:  _screenSize.height * 0.1,
                      child: new Container(
                            color: Colors.black12,
                            child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new SizedBox(
                                width: 15,
                              ),
                              new IconButton(
                                icon: Icon(Icons.delete, color: Colors.white,),
                                onPressed: () {},
                                tooltip: 'Delete',
                              ),
                              new IconButton(
                                icon: Icon(Icons.favorite, color: Colors.white,),
                                onPressed: () {},
                                tooltip: 'Like',
                              ),
                              new IconButton(
                                icon: Icon(Icons.share, color: Colors.white,),
                                onPressed: () {},
                                tooltip: 'Share',
                              ),
                              new SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                          )
                        ),
                  ],
                )


              ) // AppBar and BottonAppBar
          ],
        ),
      ),
    ),
    );
  }




  _showImage() {
    return new Image.file(File(widget.file['path']), fit: BoxFit.cover);
  }

  _showVideo(BuildContext context) {
      var _screenSize = MediaQuery.of(context).size;
      return _videoPlayerController != null && _videoPlayerController.value.initialized
          ? new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new SizedBox(height: _screenSize.width * 0.04,),
                new SizedBox(
                  width: _screenSize.width * 0.85,
                  height: _screenSize.height * 0.75,
                  child:  new AspectRatio(
                    //aspectRatio: 1,
                    aspectRatio: _videoPlayerController.value.aspectRatio,
                    child: VideoPlayer(_videoPlayerController),
                  ),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new SizedBox(width: 30),
                    new IconButton(
                      icon: Icon(
                        Icons.volume_up,
                        size: 36,
                        color: Colors.grey,
                      ),
                      onPressed:() async {
                        await _videoPlayerController.pause();
                        await _videoPlayerController.seekTo(new Duration(milliseconds: 0) );
                      },
                    ),
                    new SizedBox(width: 10),
                    new IconButton(
                      icon: Icon(
                        Icons.stop,
                        size: 36,
                        color: Colors.red,
                      ),
                      onPressed:() async {
                        await _videoPlayerController.pause();
                        await _videoPlayerController.seekTo(new Duration(milliseconds: 0) );
                      },
                    ),
                    new IconButton(
                      icon: Icon(
                        _isPlaying ? Icons.pause : Icons.play_arrow,
                        size: 36,
                        color: _isPlaying ?  Colors.blue :  Colors.green
                      ),
                      onPressed:() async {
                        if(_isPlaying) {
                          await _videoPlayerController.pause();
                        } else {
                          await _videoPlayerController.play();
                        }
                      },
                    ),
                    new SizedBox(width: 10),
                    new IconButton(
                      icon: Icon(
                        Icons.volume_down,
                        size: 36,
                        color: Colors.grey,
                      ),
                      onPressed:() async {
                        await _videoPlayerController.pause();
                        await _videoPlayerController.seekTo(new Duration(milliseconds: 0) );
                      },
                    ),
                    new SizedBox(width: 30,),
                  ],
                ),
              ],
            )
          : new CircularProgressIndicator()
          ;
  }

}