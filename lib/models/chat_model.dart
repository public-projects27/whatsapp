

class ChatModel {
  final int id;
  final String name;
  final String message;
  final String time;
  final String avatarUrl;

  ChatModel({this.id ,this.name, this.message, this.time, this.avatarUrl});
}

List<ChatModel> dummyData = [
  new ChatModel(
    name: 'علیرضا جمالی',
    message: 'ساعت 10 منتظرتم!',
    time: '12:00PM,Today',
    avatarUrl: 'https://cdn.iconscout.com/icon/free/png-512/man-1420689-1200959.png'
  ),
  new ChatModel(
    name: 'مریم ایزدی',
    message: 'سلام. خوبی؟',
    time: '11:27AM,Today',
    avatarUrl: 'https://cdn.iconscout.com/icon/free/png-512/girl-1420684-1200956.png'
  ),
  new ChatModel(
    name: 'علی زمانی',
    message: 'داداش مرسی',
    time: '09:11PM,22 July',
    avatarUrl: 'https://cdn.iconscout.com/icon/free/png-512/man-1420685-1200957.png'
  ),
  new ChatModel(
      name: 'یاسین شکیبا',
      message: 'داداش میشه فایلو بفرستی واسم؟!',
      time: '01:58PM,18 July',
      avatarUrl: 'https://cdn.iconscout.com/icon/premium/png-512-thumb/boy-564-1129347.png'
  ),
  new ChatModel(
      name: 'صبا احمدی',
      message: 'از شما ممنونم.',
      time: '10:32َM,18 July',
      avatarUrl: 'https://cdn.iconscout.com/icon/free/png-512/girl-1420686-1200958.png'
  ),
  new ChatModel(
      name: 'اکبر دستان',
      message: 'خواهش میکنم انجام وظیفه س',
      time: '09:42PM,17 July',
      avatarUrl: 'https://cdn.iconscout.com/icon/free/png-512/man-1420683-1200955.png'
  ),
  new ChatModel(
      name: 'زهرا مولوی',
      message: 'سلام بابت آگهی تون مزاحم شدم.',
      time: '08:50PM,15 July',
      avatarUrl: 'https://cdn.iconscout.com/icon/premium/png-512-thumb/female-123-194604.png'
  ),
];