
import 'package:json_annotation/json_annotation.dart';


/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'Product.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()

class Product {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'user_id')
  int userId;
  @JsonKey(name: 'title')
  String title;
  @JsonKey(name: 'body')
  String body;
  @JsonKey(name: 'image')
  String image;
  @JsonKey(name: 'price')
  String price;
  @JsonKey(name: 'created_at')
  String createdAt;
  @JsonKey(name: 'updated_at')
  String updatedAt;


  Product(this.id, this.userId, this.title, this.body, this.image, this.price,
      this.createdAt, this.updatedAt);


  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);

  @override
  String toString() {
    return 'product{id: $id, user_id: $userId, title: $title, image: $image, price: $price, created_at: $createdAt, updated_at: $updatedAt}';
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic> {
      'id': id,
      'user_id': userId,
      'title': title,
      'body': body,
      'image': image,
      'price': price,
      'created_at': createdAt,
      'updated_at': updatedAt
    };
  }
}
